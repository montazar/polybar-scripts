#!/usr/bin/env bash

this="$(echo "$0" | grep -o '/\w*' |  tail -1 | cut -d/ -f2)"
used="$(free -m | head -2 | tail -1 | awk '/[0-9]+/ {print $3}' | sed 's/[a-zA-Z]*//g')"
available=$(free -m | head -2 | tail -1 | awk '/[0-9]+/ {print $2}' | sed 's/[a-zA-Z]*//g')

if [ ! -f "/tmp/$this.settings" ]; then
    echo "percentage" > "/tmp/$this.settings"
fi

current="$(cat "/tmp/$this.settings")"

while :; do
    case $1 in
	--percentage)
	    if [ "$current" = "percentage" ]; then 
		printf "%.0f%%\n" "$(echo "( $used / $available ) * 100" | bc -l)"
	    else
		printf "%.0f/%.0f GB\n" "$(echo "$used / 1000" | bc -l)" "$(echo "$available / 1000" | bc -l)"
	    fi
	    exit 0
	    ;;
	--integer)
	    printf "%.0f/%.0f GB\n" "$(echo "$used / 1000" | bc -l)" "$(echo "$available / 1000" | bc -l)"
	    exit 0
	    ;;
	--toggle)
	    if [ "$current" = "percentage" ]; then
		echo "integer" > "/tmp/$this.settings"
	    else
		echo "percentage" > "/tmp/$this.settings"
	    fi
	    exit 0
	    ;;
	--)
	    echo "No option."
	    exit 1
	    ;;
	*)
	    echo "Invalid option."
	    exit 1;
    esac
done
