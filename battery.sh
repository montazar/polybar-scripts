#!/usr/bin/env bash

# ICONS
ICON_FULL=''
ICON_ALMOST_FULL=''
ICON_HALF=''
ICON_ALMOST_EMPTY=''
ICON_EMPTY=''
ICON_CHARGING=''

if command -v dunstify &>/dev/null; then
    notify="dunstify --replace 110102020"
else
    notify="notify-send"
fi

if [ -f /tmp/battery-toggle ]; then
    if acpi | grep -q 'Unknown'; then
        # Running on AC power, and battery is not being charged.
        $notify "AC Power" "Not charging..."
        rm -f /tmp/battery-toggle
        exit 0

    elif acpi | grep -q 'Charging'; then
       
        time_to_full=$(acpi | awk '/Charging/ {print $5}')
        if [ -n "$time_to_full" ]; then
            hours="$(echo "$time_to_full" | cut -d: -f1 | sed 's/^0*//')"
            minutes="$(echo "$time_to_full" | cut -d: -f2 | sed 's/^0*//')"
        else
            time_to_full=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | awk -F: '/time to full/ {print $2}' | tr -cd '[:digit:].,')
            hours=$(echo "$time_to_full" | cut -d, -f1)
            minutes=$(echo "60 * 0.$(echo "$time_to_full" | cut -d, -f2)" | bc | cut -d. -f1)
        fi
    else
        time_to_empty=$(acpi | awk '/Discharging/ {print $5}')
        if [ -n "$time_to_empty" ]; then
            hours="$(echo "$time_to_empty" | cut -d: -f1 | sed 's/^0*//')"
            minutes="$(echo "$time_to_empty" | cut -d: -f2 | sed 's/^0*//')"
        else
            time_to_empty=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | awk -F: '/time to empty/ {print $2}' | tr -cd '[:digit:].,')
            hours=$(echo "$time_to_empty" | cut -d, -f1)
            minutes=$(echo "60 * 0.$(echo "$time_to_empty" | cut -d, -f2)" | bc | cut -d. -f1)
        fi
    fi

    if [ "$hours" -gt "0" ] && [ "$minutes" -gt "0" ]; then
        message="$hours hours and" 
    elif [ "$hours" -gt "0" ] && [ "$minutes" -le "0" ]; then
        message="$hours hours"
    fi

    if [ "$minutes" -le "1" ]; then
        message="${message} $minutes minute"
    elif [ "$minutes" -gt "1" ]; then
        message="${message} $minutes minutes"
    fi

    echo "$message"
    $notify "Remaining time" "$message"

    rm -f /tmp/battery-toggle
    exit 0
fi

battery="$(sudo tlp-stat -b | tac | awk '/Charge/ {print $3; exit}' | awk -F. '{print $1}')"

if acpi | grep -q 'Charging'; then

    echo -n "$ICON_CHARGING"
else
    if command -v tp-battery-mode &>/dev/null; then
        FULL=$(awk -F= '/STOP_THRESHOLD/ {print $2}' < /etc/tp-battery-mode.conf )

        CHARGE_ALMOST_FULL=$(echo "$FULL * 0.75" | bc | awk -F. '{print $1}')
        CHARGE_HALF=$(echo "$FULL * 0.50" | bc | awk -F. '{print $1}')
        CHARGE_ALMOST_EMPTY=$(echo "$FULL * 0.25" | bc | awk -F. '{print $1}')

        if [ "$battery" -ge "$FULL" ]; then
            echo -n "$ICON_FULL"
        elif [ "$battery" -ge "$CHARGE_ALMOST_FULL" ]; then
            echo -n "$ICON_ALMOST_FULL"
        elif [ "$battery" -ge "$CHARGE_HALF" ]; then
            echo -n "$ICON_HALF"
        elif [ "$battery" -ge "$CHARGE_ALMOST_EMPTY" ]; then
            echo -n "$ICON_ALMOST_EMPTY"
        elif [ "$battery" -lt "5" ]; then
            echo -n "$ICON_EMPTY"
        fi
    else

        if [ "$battery" -ge "100" ]; then
            echo -n "$ICON_FULL"
        elif [ "$battery" -ge "75" ]; then
            echo -n "$ICON_ALMOST_FULL"
        elif [ "$battery" -ge "50" ]; then
            echo -n "$ICON_HALF"
        elif [ "$battery" -ge "25" ]; then
            echo -n "$ICON_ALMOST_EMPTY"
        elif [ "$battery" -lt "5" ]; then
            echo -n "$ICON_EMPTY"
        fi
    fi
fi

echo " $battery%"

