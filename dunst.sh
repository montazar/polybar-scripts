#!/usr/bin/env sh

ICON_MUTED=""
ICON_UNMUTED=""

if [ "$1" = "toggle" ]; then
    dunstctl set-paused toggle
fi

if $(dunstctl is-paused) = "false"; then
    echo $ICON_MUTED
else
    echo $ICON_UNMUTED
fi
