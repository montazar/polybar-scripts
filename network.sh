#!/usr/bin/env bash

# The default unit is bytes if none is provided. If both bits and bytes is selected, whichever comes last is selected
UNIT=1
UNIT_NAME="bytes"

# Unless specified, default to accumulate stats from all interfaces
INTERFACES=$(ls -h /sys/class/net)

# By default, if a single interface is used, every subsequent single interface will overwrite the previous if more than one is defined
MULTIPLE_INTERFACES=1

ICON_UPLOAD=""
ICON_DOWNLOAD=""

function usage() {
    echo "Measure throughput per second on a single or all network interfaces."
    echo
    echo "Usage: network [OPTION]"

    echo
    echo "Options:"
    echo " -b, --bits 			measure in bits"
    echo " -B, --bytes 			measure in bytes"
    echo " 	--if <interface> 	monitor only a single interface"
    echo " --all 			monitor all interfaces"
    echo " --list 			list all available interfaces"
    echo " -h, -H, --help 		print this manual"

    echo "Examples:"
    echo " network 		Use default behavior"
    echo " network --all -b 	Monitor all interfaces, measure in bits"
}

options=$(getopt -n "$0" -o bBhHsv:M --long bits,bytes,if:,all,help,version,list,multi -- "$@")
eval set -- "$options"
while :; do
    case $1 in
	-b|--bits)
	    UNIT=8
	    UNIT_NAME="bits"
	    shift; continue
	    ;;
	-B|--bytes)
	    UNIT=1
	    UNIT_NAME="bytes"
	    shift; continue
	    ;;
	-h|-H|--help)
	    usage
	    exit 0
	    ;;
	-v|--version)
	    echo 'network'
	    exit 0
	    ;;
	--if)
	    SINGLE_INTERFACE="$2"
	    shift 2; continue
	    ;;
	--all)
	    INTERFACES="$(ls -h /sys/class/net)"
	    shift; continue
	    ;;
	--list)
	    echo "Available interfaces: "
	    echo "$(ls -h /sys/class/net)"; exit 0
	    ;;
	-M|--multi)
	    MULTIPLE_INTERFACES=0
	    shift; continue;
	    ;;
	--)
	    break
	    ;;
	*)
		usage
	    printf "Unknown option: (%s). Ignoring.\n" "$1"
	    shift; exit 1
	    ;;
    esac
done
eval set -- "$@"

# A single interface takes precedence over all. If the interface is invalid, default to all.
if [ -n "$SINGLE_INTERFACE" ] && ls -h /sys/class/net | grep -iq "$SINGLE_INTERFACE"; then
    INTERFACES="$SINGLE_INTERFACE"
elif [ -n "$SINGLE_INTERFACE" ]; then
    echo "Warning: non-existing interface ($SINGLE_INTERFACE). Defaulting to all interfaces."
fi

# rx_bytes is received, incoming
received_bytes() {
   local received=0
   local rx_bytes=0

   for interface in $INTERFACES;
   do
       rx_bytes=$(cat /sys/class/net/$interface/statistics/rx_bytes)
       received=$((received + rx_bytes))
   done

   echo $received
}

received_bytes_on_all_interfaces() {
    echo $(cat /sys/class/net/*/statistics/rx_bytes | awk '{sum += $0} END {printf "%.0f", sum}')
}

# tx_bytes is transmitted, outgoing
transmitted_bytes() {
   local received=0
   local tx_bytes=0

   for interface in $INTERFACES;
   do
       tx_bytes=$(cat /sys/class/net/$interface/statistics/tx_bytes)
       received=$((received + tx_bytes))
   done
   
   echo $received
}

transmitted_bytes_on_all_interfaces() {
    echo $(cat /sys/class/net/*/statistics/tx_bytes | awk '{sum += $0} END {printf "%.0f", sum}')
}

format() {
    local measurement=$(($1 * $UNIT))
    local unit;

    if [ "$UNIT_NAME" = "bits" ]; then
	unit='b/s'	
    else
	unit='B/s'
    fi
   
    if [ "$measurement" -ge "1000000" ]; then
	result=$(echo "$measurement / 1000000" | bc -l)
	unit="m${unit}"
	printf "%.0f %s" $result $unit 
    
    elif [ "$measurement" -ge "1000" ]; then
	result=$(echo "$measurement / 1000" | bc -l)
	unit="k${unit}"
	printf "%.0f %s" $result $unit 
    else
	printf "%.0f %s" $measurement $unit 
    fi
}

rx_before=$(received_bytes)
tx_before=$(transmitted_bytes)
sleep 1
rx_after=$(received_bytes)
tx_after=$(transmitted_bytes)

received=$((rx_after - rx_before))
transmitted=$((tx_after - tx_before))

printf "%s %-8s " $ICON_DOWNLOAD "$(format received)"
printf "%s %-8s" $ICON_UPLOAD "$(format transmitted)"

# Decrease the spacing between this polybar module and neighboring module
echo "%{O-21}"
