#!/usr/bin/env bash

LOCATION=0
YOFFSET=0
XOFFSET=0
WIDTH=18
WIDTH_WIDE=29
THEME=solarized

confirm() {
    local action=$1

    confirm="$(rofi -sep "|" -dmenu -i -p "Are you sure you want to $action?" -location $LOCATION -yoffset $YOFFSET -xoffset $XOFFSET -theme $THEME -width $WIDTH -hide-scrollbar -line-padding 4 -padding 20 -lines 2 <<< "$action|Cancel")"

    case "$confirm" in
	"$action")
	    return 0
	    ;;
	*)
	    exit 0
	    ;;
    esac
}


menu="$(rofi -sep "|" -dmenu -i -p "Power settings" -location $LOCATION -yoffset $YOFFSET -xoffset $XOFFSET -theme $THEME -width $WIDTH -hide-scrollbar -line-padding 4 -padding 20 -lines 7 <<< "Power off|Suspend|Hibernate|Hybrid-sleep|Suspend then hibernate|Reboot|Log out|Cancel")"

case "$menu" in
    "Power off")
	confirm "Power off"
	shutdown now
	;;
    "Suspend")
	confirm "Suspend"
	systemctl suspend
	;;
    "Hibernate")
	confirm "Hibernate"
	systemctl hibernate
	;;
    "Hybrid-sleep")
	confirm "Hybrid-sleep"
	systemctl hybrid-sleep
	;;
    "Suspend then hibernate")
	confirm "Suspend then hibernate"
	systemctl suspend-then-hibernate
	;;
    "Reboot") 
	confirm "Reboot"
	reboot
	;;
    "Log out") 
	confirm "Log out"
	i3-msg exit
	;;
    "Cancel") 
	exit 0
	;;
    *)	
	exit 0
	;;
esac