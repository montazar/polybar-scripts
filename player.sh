#!/usr/bin/env bash

VOLUME=2
MAX_VOLUME=150

sink=$(pacmd list-sinks | awk '/\* index:/ {print $3}')
current_volume="$(pacmd list-sinks | grep -A 15 'index: '"$sink"'' | grep 'volume:' | grep -E -v 'base volume:' | awk -F : '{print $3; exit}' | grep -o -P '.{0,3}%' | sed s/.$// | tr -d ' ')"

case "$1" in
    "toggle")
	playerctl play-pause	
	;;
    "louder")
	[ "$((current_volume + VOLUME))" -le "$MAX_VOLUME" ] && pactl set-sink-volume "$sink" +$VOLUME%
	;;
    "softer")
	pactl set-sink-volume "$sink" -$VOLUME%
	;;
    *)
	;;
esac

if playerctl status &>/dev/null && [ "$(playerctl status)" = "Playing" ]; then
    playerctl metadata --format '{{ title }} {{ duration(mpris:length) }}'
else
    echo $(date +'%a %d %B')
fi
