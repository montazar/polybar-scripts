#!/usr/bin/env bash

pkcon refresh --noninteractive -p >/dev/null 2>&1

# Retrieve number of updates
apt=$(apt list --upgradable 2>/dev/null | grep -c upgradable)
result=$([ "$apt" -gt 0 ] && echo "  $apt" || echo "")

if dpkg -l | grep -iq flatpak; then
    flatpak=$(echo "n" | flatpak update --noninteractive | tail -n +5 | head -n 2 | wc -l)
    result=$([ "$flatpak" -gt 0 ] &&  echo "$result |   $flatpak" || echo $result)
fi

echo "$result"
