#!/bin/sh
ICON=''

cpu_speed=$(sensors | grep fan1 | awk '{print $2; exit}')
gpu_speed=$(sensors | grep fan1 | awk 'NR>1 {print $2; exit}')

echo -n "$ICON "
if [ "$cpu_speed" -gt 0 ]; then
    printf "%4d" $cpu_speed
else 
    printf "%4d" 0
fi


if [ -n "$gpu_speed" ]; then
    echo -n ' / '
    printf "%4d" $gpu_speed
fi

echo