## Battery

Shows the battery charge status (based on [battery-combined-tlp](https://github.com/polybar/polybar-scripts/tree/master/polybar-scripts/battery-combined-tlp)) and can notify you about the time remaining until empty or full.

If you have [`tp-battery-mode`](https://wiki.archlinux.org/index.php/Tp-battery-mode) installed, it will check which thresholds you're using and adopt the icon accordingly.

![battery-icon](screenshots/battery.png)

### Dependencies

- `tlp`
- `tp-battery-mode` (optional)

### Configuration

**Manually:** You have to add the `tlp-stat` command to the `/etc/sudoers` NOPASSWD of your user:

```bash
<your usename> ALL=(ALL) NOPASSWD: /usr/bin/tlp-stat
```

**Automatic:** Run this script which does it for you:

```bash
if ! sudo grep -iq '/usr/bin/tlp-stat' /etc/sudoers; then
sudo tee -a /etc/sudoers << EOF
$USER ALL=(ALL) NOPASSWD: /usr/bin/tlp-stat
EOF
```

### Module

```bash
[module/battery]
type = custom/script
exec = ~/polybar-scripts/battery.sh
interval = 5
click-left = touch /tmp/battery-toggle && ~/.config/scripts/battery.sh
```

